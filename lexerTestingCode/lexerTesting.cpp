#include <iostream>
#include <fstream>
#include <string>
#include "lexer.hpp"

using namespace std;

int test(string input){
    vector<token> tokens = lex(input);
    for(int i = 0; i < tokens.size(); i++){
        
        if (tokens[i].t == type::id){
            cout << "id" << " ";
        }else if (tokens[i].t == type::num){
            cout << "num" << " ";
        }else if (tokens[i].t == type::string){
            cout << "string" << " ";
        }else if (tokens[i].t == type::op){
            cout << "op" << " ";
        }else if (tokens[i].t == type::comment){
            cout << "comment" << " ";
        }else if (tokens[i].t == type::newline){
            cout << "newline" << " ";
        }else if (tokens[i].t == type::eof){
            cout << "eof" << " ";
        }else if (tokens[i].t == type::unknown){
            cout << "unknown" << " ";
        }
        cout <<tokens[i].v << endl;

    }
    return 0;
}

int main(){
    std::ifstream t("lexerTestCases.txt");
    std::string testCases;

    t.seekg(0, std::ios::end);   
    testCases.reserve(t.tellg());
    t.seekg(0, std::ios::beg);

    testCases.assign((std::istreambuf_iterator<char>(t)),
                std::istreambuf_iterator<char>());

    size_t pos = 0;
    std::string token;
    string delimiter = "-----";
    while ((pos = testCases.find(delimiter)) != std::string::npos) {
        token = testCases.substr(0, pos);
        test(token);
        testCases.erase(0, pos + delimiter.length());
    }
    return 0;
}