#ifndef lexer_hpp
#define lexer_hpp

#include <string>
#include <vector>

using namespace std;

enum class type{
        id,
        num,
        string,
        boolean,
        op,
        comment,
        newline,
        eof,
        unknown
};

struct token{ 
    type t;
    string v;
};

vector<token> lex(string input);

#endif