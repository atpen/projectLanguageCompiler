#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
#include "lexer.hpp"

using namespace std;



vector<token> lex(string input){
    vector<token> tokens;
    vector<string> idList = {"print", "if", "goto", "var", "def"};
    vector<char> opList = {'+', '-', '*', '/', '=', '>', '<'};
    vector<char> numList = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    vector<string> boolList = {"true", "false"};

    istringstream ss(input);
    string word;
    bool inString = false;
    bool inComment = false;
    token temp;
    while(ss >> word){
        //checks if word is a continued string
        if(inString){
            temp.t = type::string;
            temp.v += " " + word;;
            if(word[word.length()-1] == '"'){
                inString = false;
                tokens.push_back(temp);
                temp.v = "";
                temp.t = type::unknown;
            }else{
                inString = true;
            }
            continue;
        }

        //checks if word is a continued comment
        if (inComment){
            temp.t = type::comment;
            temp.v += " " + word;
            if(word[word.length()-1] == '\n'){
                inComment = false;
                tokens.push_back(temp);
                temp.v = "";
                temp.t = type::unknown;
            }else{
                inComment = true;
            }
            continue;
        }

        //checks if word is a comment, string, number, newline, id, operator, boolean, or unknown
        if(word[0] == '#'){
            temp.t = type::comment;
            temp.v += word;
            if(word[word.length()-1] == '\n'){
                inComment = false;
            }else {
                inComment = true;
                continue;
            }
        }else if(word[0] == '\"'){
            temp.t = type::string;
            temp.v += word;
            if(word[word.length()-1] == '\"'){
                inString = false;
            }else{
                inString = true;
                continue;
            }
        }else if(find(numList.begin(), numList.end(), word[0]) != numList.end()){
            temp.t = type::num;
            temp.v = word;
        }else if(word[0] == '\n' or word[0] == ';'){
            temp.t = type::newline;
            temp.v = word;
        }else if(find(idList.begin(), idList.end(), word) != idList.end()){
            temp.t = type::id;
            temp.v = word;
        }else if(find(opList.begin(), opList.end(), word[0]) != opList.end()){
            temp.t = type::op;
            temp.v = word;
        }else if(find(boolList.begin(), boolList.end(), word) != boolList.end()){
            temp.t = type::boolean;
            temp.v = word;
        }else{
            temp.t = type::unknown;
            temp.v = word;
        }
        tokens.push_back(temp);
        temp.v = "";
        temp.t = type::unknown;
    }
    return tokens;
}